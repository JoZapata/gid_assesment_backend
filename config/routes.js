exports.default = {
  routes: () => ({
    get: [
      { path: '', action: 'redirectToWeb' },
      { path: 'system/status', action: 'systemStatus' },
      { path: 'opportunities', action: 'GIDGetOpportunities' },
      { path: 'opportunities/:id', action: 'GIDGetOpportunity' }
    ],
    post: [
      { path: 'customers', action: 'customerList' }
    ],
    put: [],
    patch: [
      { path: 'opportunities/:id', action: 'GIDUpdateOpportunity' }
    ],
    delete: [
      { path: 'customers/external_accounts/:id', action: 'appCustomersExternalAccountsDelete' }
    ]
  })
};
