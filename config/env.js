const fs = require('fs');

const setProperEnv = (file) => {
  let fileExists = true;
  
  try {
    fs.accessSync(`${process.cwd()}/env/${file}`, 'r');
  } catch (e) {
    fileExists = false;
  }
  
  if (!fileExists) {
    // eslint-disable-next-line
    throw new Error(`Missing environment file ${file} does not exist`);
  }
  
  // eslint-disable-next-line
  return require('dotenv').config({path: `${process.cwd()}/env/${file}`});
};

exports.default = {
  init: () => {
    return setProperEnv('.env.development');
    
    // throw new Error('Missing environment file');
  }
};
