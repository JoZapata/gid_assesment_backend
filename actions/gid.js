/* eslint-disable class-methods-use-this */
const { api, Action } = require('actionhero')

exports.GIDGetOpportunities = class GID extends Action {
  constructor () {
    super()
    this.name = 'GIDGetOpportunities'
    this.description = 'get opportunities from expa'

    this.inputs = {
      queryText: { required: false },
      foodCovered: { required: false },
      transportationCovered: { required: false },
      accomodationCovered: { required: false }
    }
  }

  async run ({ params, response, connection }) {
    const res = await api.gid.getOpportunities({ params, connection })
    response.data = res
  }
}

exports.GIDGetOpportunity = class GID extends Action {
  constructor () {
    super()
    this.name = 'GIDGetOpportunity'
    this.description = 'get opportunity from expa'

    this.inputs = {
      id: { required: true }
    }
  }

  async run ({ params, response, connection }) {
    const res = await api.gid.getOpportunity({ params, connection })
    response.data = res
  }
}

exports.GIDUpdateOpportunity = class GID extends Action {
  constructor () {
    super()
    this.name = 'GIDUpdateOpportunity'
    this.description = 'update opportunity from expa'

    this.inputs = {
      id: { required: true },
      diff: { required: true }
    }
  }

  async run ({ params, response, connection }) {
    const res = await api.gid.updateOpportunity({ params, connection })
    response.data = res
  }
}
