/* eslint-disable class-methods-use-this */
const { Initializer, api } = require('actionhero')
const axios = require('axios')

const endpoint = 'http://api-staging.aiesec.org/v2/'
const token = 'access_token=dd0df21c8af5d929dff19f74506c4a8153d7acd34306b9761fd4a57cfa1d483c'

module.exports = class SupportInitializer extends Initializer {
  /**
   * @constructor()
   */
  constructor () {
    super()
    this.name = 'branch'
    this.loadPriority = 1100
    this.startPriority = 1100
    this.stopPriority = 1100
  }

  async initialize () {
    api.gid = {}

    api.gid.getOpportunities = async ({ params, connection }) => {
      const method = 'opportunities'

      let query = ''

      var filters = {
        foodCovered: 'food_covered',
        transportationCovered: 'transportation_covered',
        accomodationCovered: 'accommodation_covered'
      }

      Object.keys(params).forEach((key, index) => {
        if (
          (key === 'queryText') &&
          (params[key] !== '')
        ) {
          query = `${query}&q=${params[key]}`
        } else if
        (
          (
            key === 'foodCovered' ||
            key === 'transportationCovered' ||
            key === 'accomodationCovered'
          ) &&
          (params[key] !== 'disabled')
        ) {
          const val = params[key] === 'active'
          query = `${query}&filters[${filters[key]}]=${val}`
        }
      })

      const url = `${endpoint}${method}?${token}${query}`
      const res = await axios.get(url, {})

      return res.data
    }

    api.gid.getOpportunity = async ({ params, connection }) => {
      const method = `opportunities/${params.id}`
      const url = `${endpoint}${method}?${token}`
      const res = await axios.get(url, {})

      return res.data
    }

    api.gid.updateOpportunity = async ({ params, connection }) => {
      const {
        id,
        diff
      } = params

      const method = `opportunities/${params.id}`

      let json = {
        access_token: 'dd0df21c8af5d929dff19f74506c4a8153d7acd34306b9761fd4a57cfa1d483c',
        opportunity: {}
      }
      Object.keys(diff).forEach((key, index) => {
        json.opportunity[`${key}`] = diff[key]
      })

      try {
        const url = `${endpoint}${method}`
        console.log(url)
        console.log(json)
        const res = await axios.patch(url, json)
        if (typeof (res.data) === 'string') {
          throw Error('Error while making HTTP Patch request')
        } else {
          return diff
        }
      } catch (error) {
        console.log(error)
        const msg = 'Error while making HTTP request'
        console.log(msg)
        throw Error(msg)
      }
    }
  }
}
